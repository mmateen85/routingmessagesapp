/**
 * 
 */
package com.server;

import java.net.ServerSocket;
import java.net.Socket;


/**
 * @author mmateen
 *
 */

public class Server {
    
    private static ServerSocket server;

	/**
     * 
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
    	
    	server = new ServerSocket(7788);
    	System.out.println("\r\nServer started on port 7788... ");
    	System.out.println();
    	
    	while (true) {
    		Socket client = server.accept();
        	new Connection(client);
    	}

    }


}