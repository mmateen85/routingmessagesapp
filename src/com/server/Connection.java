/**
 * 
 */
package com.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * @author mmateen
 *
 */
public class Connection implements Runnable {
	
	//private String[] recipients = {"Viorica Dancila".toLowerCase(), "Liviu-Dragnea".toLowerCase(), "Radu".toLowerCase()};
	private String[] recipients; // = {"Mateen".toLowerCase(), "Liviu-Dragnea".toLowerCase(), "Radu".toLowerCase()};
	private int nextHubPort = 0;
	
	private Socket socket;
	private BufferedReader reader;
	private PrintWriter writer;
	private Socket nextHubSocket;
	
	public Connection(Socket socket) throws IOException {
		this.socket = socket;
		this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		this.writer = new PrintWriter(socket.getOutputStream());
		//this.recipients = new String[]{"Viorica Dancila".toLowerCase(), "Liviu-Dragnea".toLowerCase(), "Radu".toLowerCase()};
		this.recipients = new String[]{"Mateen".toLowerCase(), "Liviu-Dragnea".toLowerCase(), "Radu".toLowerCase()};
		new Thread(this).start();
		 
	}

	@Override
	public void run() {
		
		String data = null;
		try {
			writer.write("Enter Message: ");writer.flush();
			while ( socket!=null && !socket.isClosed() && (data = reader.readLine()) != null ) {
			    System.out.println(data);
			    
			    // assuming message format: sender recipient subject message
			    String[] parts = data!=null?data.trim().split("\\s"):null;
			    if (parts != null && parts.length < 4) {
			    	System.out.println("Hey, atleast four words are required in the message.");
			    	writer.write("Hey, atleast four words are required in the message.\r\n\r\nEnter Message: ");writer.flush();
			    	continue;
			    }
			    System.out.println(Arrays.deepToString(parts));
			    // check if recipient is from its recipient list
			    if (parts[1] == null || !search(parts[1])) {
			    	if (nextHubPort == 0) {
			    		System.out.println("Hey, the recipient does not appear in my list. Also next Hub Server port is 0.");
			    		writer.write("Hey, the recipient does not appear in my list. Also next Hub Server port is 0.\r\n\r\nEnter Message: ");writer.flush();
			    		continue;
			    	}
			    	// writer.write("Hey, the recipient does not appear in my list.\r\n\r\nEnter Message: ");writer.flush();

					try {
						this.nextHubSocket = new Socket(socket.getInetAddress(), nextHubPort);
						Thread.sleep(1000);
						PrintWriter nextHubWriter = new PrintWriter(nextHubSocket.getOutputStream());
						nextHubWriter.println(data);
						nextHubWriter.flush();
						nextHubWriter.close();
						System.out.println("Message sent to next Hub server on Port: " + nextHubPort);
						writer.write("Message sent to next Hub server on Port: " + nextHubPort+"\r\n\r\nEnter Message: ");writer.flush();
						continue;
					} catch (Exception ex) {
						ex.printStackTrace();
						writer.write("Hey, the recipient does not appear in my list. And it seems next Hub Server is also not available on Port: "+nextHubPort+"\r\n\r\nEnter Message: ");writer.flush();
			    		continue;
					}

			    }
			    
			    System.out.println("Message accepted!");
			    writer.write("Message accepted!");writer.flush();
			    
			    // it will save the message in a local directory with the recipient's name, 
			    // generating a file name based on the time the message was saved;
			    File dir = new File(parts[1]);
			    dir.mkdir();
			    
			    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
			    File file = new File(dir, df.format(Calendar.getInstance().getTime()));
			    
			    PrintWriter fileWriter = new PrintWriter(file);
			    fileWriter.append(data);
			    fileWriter.flush();
			    fileWriter.close();
			    
			    writer.write("\r\n\r\nEnter Message: ");writer.flush();
			}
		} catch (IOException e) {
			
		}
		
	}
	
	private boolean search(String recipient) {
		for (String string : this.recipients) {
			if (string.trim().equalsIgnoreCase(recipient.trim())) {
				return true;
			}
		}
		return false;
	}

}
